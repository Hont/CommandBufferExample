﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CommandBuffer_Example1 : MonoBehaviour
{
    public Material ballMaterial;
    public Renderer ballRenderer;

    CommandBuffer mCacheCommandBuffer;
    CommandBuffer mCacheCommandBuffer2;


    void OnEnable()
    {
        mCacheCommandBuffer = new CommandBuffer();
        mCacheCommandBuffer.name = "TestCommandBuffer";

        mCacheCommandBuffer.DrawRenderer(ballRenderer, ballMaterial, 0, 3);

        Camera.main.AddCommandBuffer(CameraEvent.AfterGBuffer, mCacheCommandBuffer);

        mCacheCommandBuffer2 = new CommandBuffer();
        mCacheCommandBuffer2.name = "TestCommandBuffer2";
        mCacheCommandBuffer2.DrawRenderer(ballRenderer, ballMaterial, 0, 0);
        Camera.main.AddCommandBuffer(CameraEvent.AfterLighting, mCacheCommandBuffer2);
    }

    void OnDisable()
    {
        Camera.main.RemoveCommandBuffer(CameraEvent.BeforeImageEffects, mCacheCommandBuffer);
        mCacheCommandBuffer.Dispose();

        Camera.main.RemoveCommandBuffer(CameraEvent.BeforeImageEffects, mCacheCommandBuffer2);
        mCacheCommandBuffer2.Dispose();
    }
}
